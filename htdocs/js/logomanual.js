/// JS stranky single-studie >>

$(document).ready(function() {
	animateHashLink();
	copyToClipboard();
	handleMobileMenu();
 
}); //document.ready() END

//Plynulé Scrollování kotev
function animateHashLink() {
    $('.logomanual a[href^="#"]').on('click',function (e) {
        e.preventDefault();

        if($(window).width() < 950) $('.logomanual__hamburger.close').trigger('click');

        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });

  });
}

function copyToClipboard(){
	$('.logomanual .nav a[href="#copy"]').click(function() {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val('http://www.ifsystem.cz/logomanual').select();
		document.execCommand("copy");
		$temp.remove;
	});

}

function handleMobileMenu() {
	$('.logomanual__hamburger').click(function() {
		if($(this).hasClass('close')){
			$(this).removeClass('close');
			$(this).removeClass('x');
			$('.logomanual .logomanual__leftside').removeClass('active');
			$('.logomanual .page--logomanual').removeClass('shifted');
		}else {
			$(this).addClass('close');
			$(this).addClass('x');
			$('.logomanual .logomanual__leftside').addClass('active');
			$('.logomanual .page--logomanual').addClass('shifted');
		}
	});
}