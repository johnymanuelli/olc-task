První metoda vytvořených byznys karet je vytvořena pomocí CSS clip-path
    - Bohužel pouze v google chrome vznikají problémy ( Zobrazuje se oranžová čára po obvodu karty )

Druhá metoda je vytvořena pomocí position absolute a pseudo elementů
    - Zobrazuje se správně na všech prohlížečích
    - BG a stíny jsou exportovány z .ai
    
Pro dodržení aspect ratio při reponsivitě je použitá jednoduchá funkce v commonFunctions.js, kdy dělíme šířku vizitky ( 50% - 2rem ) aspect ratiem 1.8
    a při každém resize se propočítává výška znovu.
