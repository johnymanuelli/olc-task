$(document).ready(function() {

	$('.js-nav-heading').click(function() {
		$(this).addClass('open');
	});

	$('.js-nav-heading').mouseleave(function() {
		$(this).removeClass('open');
	});



	$('.js-nav-products').click(function() {
		$('#products-menu').fadeToggle();
	});
	$('#products-menu .close').click(function() {
		$('#products-menu').fadeOut();
	});


	$.get("/images/products.svg", function(data) {
		var innerHTML = new XMLSerializer().serializeToString(data.documentElement);
			$('.js-load-products-ilu').html(innerHTML);

		$('.js-load-products-ilu .highlighted').mouseenter(function() {
			$('.products-menu__link[data-group="'+$(this).data('item')+'"]').addClass('hovered');
		});
		$('.js-load-products-ilu .highlighted').click(function() {
			$('.products-menu__link[data-group="'+$(this).data('item')+'"]')[0].click();
		});

		$('.js-load-products-ilu .highlighted').mouseleave(function() {
			$('.products-menu__link[data-group="'+$(this).data('item')+'"]').removeClass('hovered');
		});
		$('.js-load-products-ilu .highlighted').click(function() {
			$('.products-menu__link[data-group="'+$(this).data('item')+'"]')[0].click();
		});
	});

	$('.products-menu__link').mouseenter(function() {
		$('.js-load-products-ilu .highlighted[data-item='+$(this).data('group')+']').addClass('active');
	});

	$('.products-menu__link').mouseleave(function() {
		$('.js-load-products-ilu .highlighted[data-item='+$(this).data('group')+']').removeClass('active');
	});

});