function handleWideTallImages() {
	$('img').each(function(){
		setImage($(this));
 	});
}

function setImage($img) {
	var h = $img.height();
	var w = $img.width();
	var parent_h = $img.parent().height();
	var parent_w = $img.parent().width();

	if((w/h) < (parent_w/parent_h)) {
		imgClass='taller';
	}else {
		imgClass='wider';
	}

	$img.addClass(imgClass);



}