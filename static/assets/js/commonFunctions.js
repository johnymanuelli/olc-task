jQuery(document).ready(function($) {

  $(window).scroll(function() {
    var scrollPos = $(window).scrollTop(),
        navbar = $('.header__menu-container');

    if (scrollPos > 10) {
      navbar.addClass('scrolled');
    } else {
      navbar.removeClass('scrolled');
    }
  });

    $(".brand-logo").hover(function(){
        $(".brand-logo").not(this).toggleClass("faded");
    });

    $('.index__header__video').playbackRate = 0.02;

});


/***** ACCORDION START ******/
$(function() {
    var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        var links = this.el.find('.accordion-title');
        links.on('click', {
            el: this.el,
            multiple: this.multiple
        }, this.dropdown)
    }

    Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el;
        $this = $(this),
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.accordion-content').not($next).slideUp().parent().removeClass('open');
        };
    }
    var accordion = new Accordion($('.accordion-container'), false);
});
/***** ACCORDION END ******/

/***** CAROUSEL START ******/

/***** ACAROUSEL END ******/

function handleMobileMenu() {
  $('.hamburger').on('click', function() {
    //$( ".nav__link" ).addClass( "accordion-title" );

    if($('.nav__middle').hasClass('open')) {
      $('.nav__middle').removeClass('open');
      $('.hamburger').removeClass('open');
      $('body').removeClass('nav-overlayered');
    }else {
      $('.nav__middle').addClass('open');
      $('.hamburger').addClass('open');
      $('body').addClass('nav-overlayered');
    }
  });
}

$(document).ready(function() {
  handleMobileMenu();

  $(window).resize(function(){
     $('.entity--text .entity__text').each(function() {
         var $that = $(this)
         $that.removeAttr('style');
         var parent_h = $that.parent().height() - 27;
         var this_h = $that.height();
         //if(parent_h < this_h) {
              $that.css({width: $that.parent().width()+'px', height: parent_h+'px'});
              $that.textfill({
                maxFontPixels: 90,
                minFontPixels: 34,
                innerTag: 'span'
            });
     });

     // Keep .cards aspect ratio when window is resized
     $('.logomanual__htmlVizitky .card, .logomanual__tiskoviny .card').each(function(){
        var $that = $(this);
        var this_h = $that.width() / 1.8; // 1.8 is aspect ratio of business card
        $that.css({height: this_h+'px'});
     });

  });

    $(window).resize();
});

 /***** ACAROUSEL END ******/
