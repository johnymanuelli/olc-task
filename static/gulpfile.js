/**
 *
 *  Základní tasky pro vývoj projektu:
 *  ----------------------------------
 *  "gulp watch"      - sleduje změny v </static/assest/> a spouští podle tyou souboru js:dev, sass:dev, svgstore nebo sprite tasky
 *  "gulp sync"       - stejné jako watch plus spouští "browsersync" pro spuštění serveru na localhostu (defaultně na portu 3000) pro lazení na mobilech/tabletech
 *  "gulp build"      - spouští se před nasazením projektu, tasky (js:build, sass:build, clean-maps), minifikuje CSS, JS a maže sourcemaps
 *
 *
 *  Dílčí tasky (většinou obsazeny v tascích nahoře a není je potřeba spouštět samostatně
 *  -------------------------------------------------------------------------------------
 *
 *  "gulp js:dev"     - spojuje javascripty v assets/js/, vytváří sourcemaps
 *  "gulp js:build"   - spojuje javascripty v assets/js/, minifikuje, maže sourcemaps
 *
 *  "gulp sass:dev"   - spojuje sassy v assets/sass/, vytváří sourcemaps
 *  "gulp sass:build" - spojuje sassy v assets/sass/, minifikuje, maže sourcemaps
 *
 *  "gulp svgstore"   - veme všechny SVG z /assest/svg/defs/, udělá jedno defs.svg, které se skryté načítá v záhlaví stránky (typicky v basic-layout.lay.php)
 *
 *    použití takovýchto SVG je pak:
 *
 *    <svg class="tridy jake chces">
        <use xlink:href="#nazev-souboru"></use>
      </svg>
 *
 *
 *
 *
 *
 */

 'use strict';

 var gulp = require('gulp');
 var sass = require('gulp-sass');
 var sourcemaps = require('gulp-sourcemaps');
 var autoprefixer = require("autoprefixer");
 var postcss      = require('gulp-postcss');
 var uglify = require("gulp-uglify");
 var concat = require("gulp-concat");
 var livereload = require('gulp-livereload');
 var notify = require('gulp-notify');
 var sassGlob = require('gulp-sass-glob');
 var clean = require('gulp-clean');
 var watch = require('gulp-watch');
 var svgstore = require('gulp-svgstore');
 var svgmin = require('gulp-svgmin');
 var path = require('path');
 var plumber = require('gulp-plumber');
 var browserSync = require('browser-sync').create();
 var babel = require('gulp-babel');
 var addsrc = require('gulp-add-src');



 var src = "./assets/";
 var dest = "../htdocs/";

 gulp.task('sass:dev', function () {
  gulp.src(src + 'sass/style.scss')
  .pipe(sassGlob())
  .pipe(sourcemaps.init())
  .pipe(sass())
  .on('error', swallowError)
  .pipe(postcss([autoprefixer({cascade: false, grid: true})]))
  .pipe(sourcemaps.write('./maps'))
  .pipe(gulp.dest( dest + 'css/'));
});

 gulp.task('sass:build', function () {

  gulp.src(src + 'sass/style.scss')
  .pipe(sassGlob())
  .pipe(sass({outputStyle: 'compressed'}))
  .on('error', swallowError)
  .pipe(postcss([autoprefixer({cascade: false, grid: 'autoplace'})]))
  .pipe(gulp.dest( dest + 'css/'));

});

 gulp.task('svgstore', function () {
  return gulp
  .src(src + 'svg/defs/*.svg')
  .pipe(svgmin(function getOptions (file) {
    const filename = path.basename(file.relative, path.extname(file.relative));
    const removeStyle =  filename.startsWith('ico-');


    let plugins = [
      { removeViewBox: false },
      {cleanupIDs: {
          prefix: filename + '-',
          minify: true
      }}
    ];


    if(removeStyle) {
      plugins.push({convertColors: {currentColor: true}});
      plugins.push({removeStyleElement: true});
  }

//   console.log(filename + ': ' + plugins);

    return {
        plugins: plugins
        }
      }))
  .pipe(svgstore({ inlineSvg: true }))
  .pipe(gulp.dest(dest + 'images/svg/'));
});



// seznam načítaných JS, využije se pak ve variantě s uglify i bez
// vendor javascripty neprochází přes Babel
 var javascripts_vendor = src + '/js/vendor/**/*.js';
 var javascripts_user = src + '/js/*.js';

 gulp.task('js:dev', function () {
   return gulp.src(
    javascripts_user)
   .pipe(sourcemaps.init({loadMaps: true}))
   .pipe(babel({"presets": ["@babel/preset-env"]}))
   .on('error', swallowError)
   .pipe(addsrc.prepend(javascripts_vendor))
   .pipe(concat('app.js'))
   .pipe(gulp.dest(dest + 'js/'))
   .pipe(sourcemaps.write(dest + 'js/'))
   .on('error', swallowError);
 });

 gulp.task('js:build', function () {

   return gulp.src(
    javascripts_user)
    .pipe(babel({"presets": ["@babel/preset-env"]}))
   .on('error', swallowError)
   .pipe(addsrc.prepend(javascripts_vendor))
   .pipe(concat('app.js'))
   .pipe(uglify())
   .pipe(gulp.dest(dest + 'js/'))
   .on('error', swallowError);
 });

 gulp.task('watch', function() {

   livereload.listen();

   watch('assets/sass/**/*.scss', function(files) {
    gulp.start('sass:dev');
  });

   watch(dest + '/css/**/*.css', function(files) {
    livereload.changed(dest + 'css/style.css');
  });

   watch(src + '/svg/defs/*.svg', function(files) {
    gulp.start('svgstore');
    livereload.reload();
  });


   watch('assets/js/*.js', function() {
    gulp.start('js:dev');
   // livereload.reload();
  });

   watch([dest + './templates/*.*',  dest + 'js/**/*.js', '../htdocs/views/**/*.php'],  function(){
    livereload.reload();
  });

 });

 gulp.task('sync', function () {

   gulp.start('watch');

   browserSync.init({
    // proxy: 'http://automax-2020.izon.localhost/',
    proxy: 'http://olc.test/htdocs/',
    port: 3000,
    files: [dest + 'css/style.css', dest + 'index.html']
  });

 });

 gulp.task('clean-maps', function(){
  return gulp.src([dest + 'css/maps/'], {read:false})
  .pipe(clean({force:true}));
})
let plugins = [ { removeViewBox: false }];;

 gulp.task('build', ['sass:build', 'js:build', 'clean-maps']);

// řeší ukončení watch tasku při scss chybě
function swallowError (error) {
  console.log(error);

  var args = Array.prototype.slice.call(arguments);
  notify.onError({
    title: 'Compile Error',
    message: '<%= error.message %>'
  }).apply(this, args);
  this.emit('end'); // Keep gulp from hanging on this task
}
